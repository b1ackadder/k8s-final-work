# Набор артефактов для проверки финальной работы.
# Артефакты распределены по каталогам в соответствии с задачами.

### 1
  Скриншот результата выполнения команды kubectl get no -o wide https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/1/nodes.png

  Скриншот результата выполнения команды kubectl get po -A -o wide https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/1/pods.png

  Скриншот подключения к внешнему IP-адресу вашего NGINX ingress-контроллера с ошибкой 404 https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/1/ingress404.png

### 2
  Внешний IP-адрес frontend-сервиса или скриншот подключения к нему. https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/2/microcervices-demo.blackadder.ru.png

  Helm-чарт приложения loadgenerator. Переменные окружения должны подключаться из config map. https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/2/chart

  Скриншоты конфигурации CI/CD-пайплайна, результатов его выполнения и того, где и каким образом хранится Kubeconfig 

  пайплайн https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/2/gitlab-ci.png

  результат сборки https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/2/build1.png https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/2/build2.png

  результат деплоя https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/2/deploy.png

  хранение kubeconfig https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/2/variables.png

### 3
  Документ со списком полезных метрик и целей их сбора. https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/metrics.xlsx

  Ссылка на веб-интерфейс Grafana с именем пользователя и паролем или скриншоты, на которых видны все созданные дашборды и несколько часов исторических данных метрик.

  Использование CPU, namespace kube-system pods all https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_bxLr6Yq3P1.png

  Использование memory, namespace monitoring pods all https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_a2dAEgeBTv.png

  Использование memory, namespace app pods loadgenerator https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_cNIgbvaojU.png

  Использование CPU, namespace app pods loadgenerator https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_fxurqPWYWA.png

  Использование memory, namespace app pods all https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_GyqRkX3ukB.png

  Использование CPU, namespace all pods all https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_IpefV4H6P3.png

  Использование memory, namespace all pods all https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_rBBAeBi8c4.png

  Использование CPU, namespace monitoring pods all https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_XncMWe5VsJ.png

  Использование CPU, namespace app pods all https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/3/chrome_yMf1jBHEyo.png

### 4

  Ссылка на веб-интерфейс системы логирования с именем пользователя и паролем или скриншоты, на которых видны логи 2-3 сервисов за последние 15 минут

  Лог, namespace app, pod frontend https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/4/chrome_400ZcGarp4.png

  Лог, namespace app, pod frontend, строка поиска rpc error https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/4/chrome_vr7UEtoSqO.png

  Лог, namespace app, pod currencyservice https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/4/chrome_libSrjX2Ev.png

  Лог, namespace app, pod loadgenerator https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/4/chrome_LytYqiumCl.png

  Лог, namespace app, pod adservice time 9:07 - 9:15 https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/4/chrome_aAEPLBe7V8.png

  Лог, namespace app, pod adservice time 9:29 - 9:36 https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/4/oS7SJ1tS8K.png

### 5
  YAML-манифест HPA. https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/5/frontend-hpa.yml

### 6
  Документ с описанием доработок инфраструктуры и задач по её развитию. https://gitlab.com/b1ackadder/k8s-final-work/-/blob/main/6/plan.xlsx